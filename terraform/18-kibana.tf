resource "helm_release" "kibana" {
    name       = "kibana"
    repository = "https://helm.elastic.co"
    chart      = "kibana"
    namespace  = "monitoring"
    //create_namespace = true

    values = [
        file("${path.module}/../customs/kibana-values.yaml")
        #"${file("values.yaml")}"
    ]

    depends_on = [ helm_release.metricbeat ]
}