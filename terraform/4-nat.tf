resource "aws_eip" "nat_proj10" {
  vpc = true

  tags = {
    "Name" = "nat_proj10"
  }
}

resource "aws_nat_gateway" "nat-gw-proj10" {
  allocation_id = aws_eip.nat_proj10.id
  subnet_id = aws_subnet.public-us-east-1a.id

  tags = {
    "Name" = "nat-gw-proj10"
  }

  depends_on = [
    aws_internet_gateway.igw-proj10
  ]
}