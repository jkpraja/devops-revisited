#resource "null_resource" "kubectl" {
#    provisioner "local-exec" {
#        command = "aws eks --region ${region} update-kubeconfig --name ${aws_eks_cluster.proj9-cluster.name}"
#    }
#}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

resource "helm_release" "nginx-ingress-controller" {
  name       = "nginx-ingress-controller"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  namespace  = "ingress-nginx"
  create_namespace = true

  depends_on = [null_resource.update_kubeconfig]
}