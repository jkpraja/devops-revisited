resource "helm_release" "kubecost" {
    name       = "kubecost"
    repository = "https://kubecost.github.io/cost-analyzer"
    chart      = "cost-analyzer"
    namespace  = "kubecost"
    create_namespace = true

    values = [
        file("${path.module}/../customs/kubecost-values.yaml")
        #"${file("values.yaml")}"
    ]

    # depends_on = [ helm_release.grafana ]
}