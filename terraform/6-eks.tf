resource "aws_iam_role" "proj10-cluster"{
  name = "proj10-cluster"

  assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "eks.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "proj10-AmazonEKSClusterPolicy"{
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role = aws_iam_role.proj10-cluster.name
}

resource "aws_eks_cluster" "proj10-cluster"{
  name = "proj10-cluster"
  role_arn = aws_iam_role.proj10-cluster.arn

  vpc_config {
    subnet_ids = [
        aws_subnet.private-us-east-1a.id,
        aws_subnet.private-us-east-1b.id,
        aws_subnet.public-us-east-1a.id,
        aws_subnet.public-us-east-1b.id
    ]
  }

  depends_on = [
    aws_iam_role_policy_attachment.proj10-AmazonEKSClusterPolicy
  ]
}

resource "null_resource" "update_kubeconfig" {
    provisioner "local-exec" {
        command = "aws eks update-kubeconfig --name ${aws_eks_cluster.proj10-cluster.name}"
    }

    depends_on = [ aws_eks_cluster.proj10-cluster ]
}

output "cluster_name" {
  description = "Kubernetes Cluster Name"
  value       = aws_eks_cluster.proj10-cluster.name
}