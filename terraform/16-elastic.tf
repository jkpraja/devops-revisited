resource "helm_release" "elastic" {
    name       = "elastic"
    repository = "https://helm.elastic.co"
    chart      = "elasticsearch"
    namespace  = "monitoring"
    create_namespace = true

    values = [
        file("${path.module}/../customs/elastic-values.yaml")
        #"${file("values.yaml")}"
    ]

    depends_on = [ aws_eks_cluster.proj10-cluster ]
}