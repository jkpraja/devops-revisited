resource "helm_release" "metricbeat" {
    name       = "metricbeat"
    repository = "https://helm.elastic.co"
    chart      = "metricbeat"
    namespace  = "monitoring"
    //create_namespace = true

    values = [
        file("${path.module}/../customs/metricbeat-values.yaml")
        #"${file("values.yaml")}"
    ]

    depends_on = [ helm_release.elastic ]
}