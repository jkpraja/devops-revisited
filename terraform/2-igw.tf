resource "aws_internet_gateway" "igw-proj10" {
  vpc_id = aws_vpc.proj10.id

  tags = {
    "Name" = "igw-proj10"
  }
}