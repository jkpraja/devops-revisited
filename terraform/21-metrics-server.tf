resource "null_resource" "metrics-server" {
    provisioner "local-exec" {
        command = "kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml"
    }

    depends_on = [ null_resource.kubectl ]
}