resource "helm_release" "jenkins" {
    name       = "jenkins"
    repository = "https://charts.jenkins.io"
    chart      = "jenkins"
    namespace  = "development"
    create_namespace = true

    values = [
        file("${path.module}/../customs/jenkins-values.yaml")
        #"${file("values.yaml")}"
    ]

    depends_on = [ aws_eks_cluster.proj10-cluster ]
}